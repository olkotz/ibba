<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Models\Protocol;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Throwable;
use Illuminate\Support\Facades\Log;

class ProtocolController extends Controller
{
    public function index(Request $request)
    {
        $patient = DB::table('users')->where('name', User::IBBA . $request->dni)->first();

        if($patient) {
            if (Protocol::where('number', cleanChars($request->number))->where('user_id', $patient->id)->exists()) {
                    $filename = $request->number . '.pdf';
                    $url = Storage::url($filename);
                    return view('protocols.pdf')->with([
                        //'url' => '/laboratorioibba.com.ar/laravel/' . $filename
                        'url' => $url
                     ]);
            } else {
                return back()->withErrors([
                    'number' => 'El Protocolo aún no ha sido Generado.',
                ]);
            }
        } else {
            return back()->withErrors([
                'dni' => 'El Paciente no se encuentra registrado.',
            ]);
        }
    }

    /**
     * Api Function to set new Users and Protocols to the App
     */
    public function newUser(Request $request)
    {
        try {
            DB::beginTransaction();
            if (!User::where('dni', $request->dni)->exists()) {
                $user = new User();
                $user->dni = $request->dni;
                $user->email = $request->email;
                $user->name = User::IBBA . $request->dni;
                $user->password = Hash::make($request->password);
                $user->save();
            }
            $user = DB::table('users')->where('dni', $request->dni)->first();
            //$user = User::where('dni', $request->dni)->get();

            if (!Protocol::where('number', $request->protocol)->exists()) {
                $protocol = new Protocol();
                $protocol->number = $request->protocol;
                $protocol->user_id = $user->id;
                $protocol->save();
            } else {
                DB::rollBack();
                return Response::json(
                    'error',
                    400
                );
            }
            DB::commit();
            return Response::json(
                'ok',
                200
            );
        } catch (Throwable $th) {
            DB::rollBack();
            return Response::json(
                $th,
                400
            );
        }
    }

}
