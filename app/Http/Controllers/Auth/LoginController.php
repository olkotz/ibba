<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Throwable;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /* public function __construct()
    {
        $this->middleware('guest')->except('logout');
    } */
    protected function guard()
    {
        return Auth::guard('guest');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'dni' => ['required'],
            'number' => ['required', 'integer'],
        ]);
        $remember = isset($request->remember) ? $request->remember : false;

        try {
            DB::beginTransaction();
            $user = User::where('email', cleanChars($request->dni) . User::EMAIL)->first();
            if(is_null($user)) {
                $user = new User;
                $user->name = User::IBBA . $request->dni;
                $user->email = User::IBBA . $request->dni . User::EMAIL;
                $user->email_verified_at = Carbon::now();
                $user->password = Hash::make($request->dni);
                $user->save();
            }
            Log::debug($user);
            DB::commit();

            Auth::login($user);

            return redirect()->route('protocols', [
                'number' => cleanChars($request->number),
                'dni' => cleanChars($request->dni)
            ]);

        } catch (Throwable $th) {
            DB::rollBack();
            return back()->withErrors([
                'number' => 'El Protocolo aún no ha sido Generado.',
            ]);
        }
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }
}
