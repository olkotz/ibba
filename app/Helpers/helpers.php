<?php

if (!function_exists('cleanChars')) {
    function cleanChars(string $dni): string
    {
        return preg_replace('([^0-9])', '', $dni);
    }
}
