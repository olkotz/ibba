<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Psy\Readline\Hoa\Protocol;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, HasFactory, HasApiTokens;
    public const EMAIL = "@gmail.com";
    public const IBBA = "test-";
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'dni', 'birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'name' => 'string',
        'lastname' => 'string',
        'email' => 'string',
        'dni' => 'string',
        'birthday' => 'date:d-m-Y',
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function protocols(): HasMany
    {
        return $this->hasMany(Protocol::class, 'user_id');
    }

    public function haveProtocol($number): User
    {
        return $this->whereHas('protocols', function (Builder $query) use ($number) {
            $query->where('number', $number);
        })->exists();
    }
}
