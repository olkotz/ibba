<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::create([
            'name' => 'admin',
            'email' => 'euskal.arima@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password'), // password
            'dni' => '25851946',
            'remember_token' => Str::random(10)
        ]);
    }
}
