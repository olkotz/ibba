@extends('dashboard.authBase')

@section('content')
    <style>
      .centered{
        text-align: center;
      }
      .ibba {
        background-color: #8d1b3f !important;
      }
      .ibba-white {
        background-color: #ffffff !important;
        color: #8d1b3f;
        &:hover {
          color: #000000;
        }
      }

    </style>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <h1 class="centered">IBBA ANÁLISIS CLÍNICOS</h1>
                <h2 class="centered">RESULTADOS</h2>
                <br>
                <p class="text-muted">Búsqueda de Resultados con DNI y Código</p>
                <br>
                <form method="POST" action="{{ route('authenticate') }}">
                    @csrf
                    <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <svg class="c-icon">
                          <use xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-user"></use>
                        </svg>
                      </span>
                    </div>
                    <input class="form-control {{$errors->has('dni') ? ' is-invalid' : ''}}" type="text" placeholder="{{ __('Documento de Identidad') }}" name="dni" value="{{ old('dni') }}" required autofocus>
                    @error('dni')
                        <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                    </div>
                    <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <svg class="c-icon">
                          <use xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-lock-locked"></use>
                        </svg>
                      </span>
                    </div>
                    <input class="form-control {{$errors->has('number') ? ' is-invalid' : ''}}" type="text" placeholder="{{ __('Código') }}" name="number" required>
                    @error('number')
                        <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <button class="btn btn-primary px-4 ibba" type="submit">{{ __('Buscar') }}</button>
                        </div>
                    </div>
                </form>
              </div>
            </div>
            <!-- <div class="card text-white bg-primary py-5 d-md-down-none ibba" style="width:44%">
              <div class="card-body text-center">
                <div>
                  <h2>Regístrese como Paciente</h2>
                  <p>Ingrese sus datos personales para futuras consultas.</p>
                  <a href="" class="btn btn-primary active mt-3 ibba-white">{{ __('Registrarse') }}</a>
                </div>
              </div>
            </div>
          </div>-->
        </div>
      </div>
    </div>

@endsection

@section('javascript')

@endsection
