@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
             
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             {{__('Protocolos')}}
                         </div>
                         <div class="card-body">
                              <div id="protocol"></div>  
                              <div class="float-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

<style>
.pdfobject-container { height: 30rem; border: 1rem solid rgba(0,0,0,.1); }
</style>
@push('scripts')
<script>PDFObject.embed("{{$url}}", "#protocol");</script>
@endpush