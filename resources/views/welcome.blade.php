<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
    <title>IBBA - Laboratorio de Análisis Clínicos</title> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <meta name="Keywords" content="laboratorio, analisis clinicos, sole" /> 
    <meta name="author" content=""/> 
    <meta name="verify-v1" content="LcbY/WgWRXpfaAj7M+WG3LOb6bOQJHIGXOtR7/dkFsA=" />
    <meta name="msvalidate.01" content="5148DA730B5EE126CA8C129B4635F8C0" /> 
    <meta name="y_key" content="ae0369c5ec93d8a0" /> 
    <meta name="description" content="IBBA - Laboratorio de Análisis Clínicos" /> 
    <meta name="robots" content="index,follow" /> 
    <meta http-equiv="content-language" content="es" /> 
    <meta name="geo.position" content="-38.727157;-62.247474" /> 
    <meta name="ICBM" content="-38.727157, -62.247474" /> 
    <style>
        .button-container{
            display:inline-block;
            position:relative;
            text-align: center;
            padding-left: 15em;
        }

        .button-container a{
            position: absolute;
            bottom:6em;
            right:9em;
            background-color:#8d1b3f;
            border-radius:1.5em;
            color:white;
            text-transform:uppercase;
            padding:1em 1.5em;
            text-decoration: none;
            font-size: 25px;
            font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
            font-weight: bold;
        }
        .button-container a:hover{
            background-color:#9a3c53;
            cursor:pointer;
            color:white;
        }
        
    </style>
</head> 
<body> 
    <div class="button-container">    
        <img src="placa_web_IBBA2.png" alt="laboratorio, analisis clinicos, sole"> 
        <a href="{{route('login')}}" alt="Resultados">RESULTADOS</a>
    </div> 
</body> 
</html>