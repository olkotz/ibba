<div class="c-subheader px-3">
    <ol class="breadcrumb border-0 m-0">
        <li class="breadcrumb-item"><a href="/">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item @if(!isset($action)) active @endif()">
            @if(isset($route))
                <a href="{{ $route }}">{{ $name }}</a>
            @else
                {{ $name }}
            @endif
        </li>
        @isset($action)
            <li class="breadcrumb-item active">{{ $action }}</li>
        @endisset
    </ol>
</div>
