<!-- Home -->
<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link" href="{{ route('home') }}">
        <span class="c-sidebar-nav-icon">
            <i class="fas fa-home"></i>
        </span>
       {{ __('menu.Home')}}
    </a>
</li>

<!-- Administration -->
@permission(['users-read', 'roles-read', 'permissions-read'])
<li class="c-sidebar-nav-title">{{__('menu.Administration')}}</li>
@endpermission

<!-- Users -->
@permission('users-read')
<li class="c-sidebar-nav-item  @if(request()->is('*users*')) active @endif">
    <a class="c-sidebar-nav-link @if(request()->is('*users*')) c-active @endif" href="{{ route('users.index') }}">
                <span class="c-sidebar-nav-icon">
                    <i class="fas fa-user-friends"></i>
                </span>
        {{__('menu.Users')}}
    </a>
</li>
@endpermission

<!-- Roles -->
@permission('roles-read')
<li class="c-sidebar-nav-item @if(request()->is('*roles*')) active @endif">
    <a class="c-sidebar-nav-link @if(request()->is('*roles*')) c-active @endif" href="{{ route('roles.index') }}">
                <span class="c-sidebar-nav-icon">
                    <i class="fas fa-chess"></i>
                </span>
       {{ __('menu.Roles')}}
    </a>
</li>
@endpermission

<!-- Permissions -->
@permission('permissions-read')
<li class="c-sidebar-nav-item @if(request()->is('*permissions*')) active @endif">
    <a class="c-sidebar-nav-link @if(request()->is('*permissions*')) c-active @endif" href="{{ route('permissions.index') }}">
            <span class="c-sidebar-nav-icon">
                <i class="fa fa-users"></i>
            </span>
        {{__('menu.Permissions')}}
    </a>
</li>
@endpermission

<!-- Patients -->
@permission('patients-read')
<li class="c-sidebar-nav-item @if(request()->is('*patients*')) active @endif">
    <a class="c-sidebar-nav-link @if(request()->is('*patients*')) c-active @endif" href="{{ route('patients.index') }}">
            <span class="c-sidebar-nav-icon">
                <i class="far fa-address-card"></i>
            </span>
        {{__('menu.Patients')}}
    </a>
</li>
@endpermission

<!-- Protocols -->
@permission('protocols-read')
<li class="c-sidebar-nav-item c-sidebar-nav-dropdown @if(request()->is('*protocols*')) c-show @endif">
    <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
        <span class="c-sidebar-nav-icon">
            <i class="fa fa-id-badge"></i>
        </span>
        {{__('menu.Protocols')}}    
    </a>
    <ul class="c-sidebar-nav-dropdown-items" style="height: auto;">
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link @if(request()->is('*protocols*')) c-active @endif"
               href="{{ route('protocols.index') }}">
                <span class="c-sidebar-nav-icon">

                </span>
                {{__('menu.List')}}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link @if(request()->is('*protocol/upload*')) c-active @endif"
               href="{{ route('protocols.uploads') }}">
                <span class="c-sidebar-nav-icon">

                </span>
                {{__('menu.Uploads')}}
            </a>
        </li>
        
    </ul>
</li>
@endpermission

<!-- Signers -->
@permission('signers-read')
<li class="c-sidebar-nav-item @if(request()->is('*signers*')) active @endif">
    <a class="c-sidebar-nav-link @if(request()->is('*signers*')) c-active @endif" href="{{ route('signers.index') }}">
            <span class="c-sidebar-nav-icon">
                <i class="fa fa-signature"></i>
            </span>
        {{__('menu.Signers')}}
    </a>
</li>
@endpermission

<!-- Developers -->
@role('superadmin')
<!-- Telescope -->
<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link" href="{{ url('cms/telescope') }}">
            <span class="c-sidebar-nav-icon">
                <i class="fa fa-telescope"></i>
            </span>
        Telescope
    </a>
</li>
@endrole
