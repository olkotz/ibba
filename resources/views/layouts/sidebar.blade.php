<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <br>
    <div class="c-sidebar-brand d-lg-down-none">
        <div class="c-sidebar-brand-full" width="118" height="46">
            <a src="{{url('/')}}" style="text-decoration: none;">
                <img src="{{ asset('images/logo-ibba-new.png') }}" height="50px" width="130px">
            </a>
        </div>
        <br>
    </div>
</div>
